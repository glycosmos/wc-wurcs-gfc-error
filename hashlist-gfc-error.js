import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class HashListGfcError extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */:

</style>

<iron-ajax auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/wurcs_select_hashlist_with_gfc_error" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <table border="1">
      <thead>
        <tr>
          <th>Hash</th>
          <th>WURCS</th>
          <th>Error message</th>
        </tr>
      </thead>
      <tbody>
        <template is="dom-repeat" items="{{sampleids}}">
          <tr>
            <td>{{item.hash}}</td>
            <td><wurcs-with-gfc-error hash_key="{{item.hash}}"></wurcs-with-gfc-error></td>
            <td><gfc-error-message hash_key="{{item.hash}}"></gfc-error-message></td>
          </tr>
        </template>
      </tbody>
    </table>
  <div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      hashed_text: {
        notify: true,
        type: String
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('hashlist-gfc-error', HashListGfcError);
