import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class RepoDetectFormat extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */:

</style>

<iron-ajax auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/repo_select_detectformat?hashed_text={{hashed_text}}" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <template is="dom-repeat" items="{{sampleids}}">
      <p>{{item.format}}</p>
    </template>
  <div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      hashed_text: {
        notify: true,
        type: String
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('repo-detectformat', RepoDetectFormat);
