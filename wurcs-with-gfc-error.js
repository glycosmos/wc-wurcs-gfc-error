import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

class WurcsWithGfcError extends PolymerElement {
  static get template() {
    return html`
<style>
/* shadow DOM styles go here */:

</style>

<iron-ajax auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/wurcs_select_wurcs_with_gfc_error?hash={{hash_key}}" handle-as="json" last-response="{{sampleids}}"></iron-ajax>
  <div>
    <template is="dom-repeat" items="{{sampleids}}">
      <p>{{item.wurcs}}</p>
    </template>
  <div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      hashed_text: {
        notify: true,
        type: String
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('wurcs-with-gfc-error', WurcsWithGfcError);
